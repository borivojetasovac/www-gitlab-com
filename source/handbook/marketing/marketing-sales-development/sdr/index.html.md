---
layout: markdown_page
title: "Sales Development"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Other pages

- [Inbound BDR handbook](/handbook/marketing/marketing-sales-development/sdr/inbound)
- [Outbound SDR handbook](/handbook/marketing/marketing-sales-development/sdr/outbound)

## Overview

As a BDR (Business Development Representative) or SDR (Sales Development Representative) you will be dealing with the front end of the sales process. Your focus will be on generating opportunities that the AEs (Account Executives) and SALs (Strategic Account Leaders) accept, ultimately leading to closed won business. On this team we work hard, but have fun too (I know, it's a cliche ...but here it's true!). We will work hard to guarantee your success if you do the same. We value results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, solutions, and quirkiness.

Being a BDR or SDR can come with what seems like long days, hard work, frustration, and even at times, failure. If you have the right mindset and come to work with a tenacious, hungry attitude you will see growth personally and in your career. GitLab is a place with endless opportunity. Let’s make it happen!

## Training and Resources

You play a crucial role that helps bridge the gap between sales and marketing. As you gain knowledge, you will be able to aid our future customers ship better software, faster. There are numerous resources at your fingertips that we have created to help you in this process.

### Process
- [Business Operations Handbook](/handbook/business-ops/)
- [Marketing Handbook](/handbook/marketing/)
- [Sales Handbook](/handbook/sales/)
- [Customer Success Handbook](/handbook/customer-success/)
- [Resellers Handbook](/handbook/resellers/)
- [Support Handbook](/handbook/support/)

### Product
- [GitLab Resources](https://about.gitlab.com/resources/)
- [GitLab Blog](https://about.gitlab.com/blog/)
- [GLU GitLab University](https://docs.gitlab.com/ee/university/)
- [GitLab Primer](https://about.gitlab.com/primer/)
- [Glossary of Terms](https://docs.gitlab.com/ee/university/glossary/README.html#what-is-the-glossary)
- [GitLab Positioning](/handbook/positioning-faq/)
- [EE Product Qualification Questions](/handbook/EE-Product-Qualification-Questions/)
- [Sales Qualification Questions](/handbook/sales-qualification-questions/)
- [GitLab Sales Demo](/handbook/sales/demo/)
- [FAQ from Prospects](/handbook/sales-faq-from-prospects/)
- [Customer Use Cases](/handbook/use-cases/)
- [Platzi GitLab Workshop](https://courses.platzi.com/classes/git-gitlab/)
- [GitLab Market and Ecosystem](https://www.youtube.com/watch?v=sXlhgPK1NTY&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e&index=6)
- [GitLab Documentation](http://docs.gitlab.com/ee/)
- [GitLab Compared to Other Tools](https://about.gitlab.com/comparison/)
- [GitLab Battlecards](https://docs.google.com/document/d/1zRIvk4CaF3FtfLfSK2iNWsG-znlh64GNeeMwrTmia_g/edit#)
- [Version.gitlab](https://version.gitlab.com/users/sign_in)
- [Customer Reference Sheet](https://docs.google.com/a/gitlab.com/spreadsheets/d/1Off9pVkc2krT90TyOEevmr4ZtTEmutMj8dLgCnIbhRs/edit?usp=sharing)
- [Additional Training Resources](https://drive.google.com/drive/folders/0B1W9hTDXggO1NGJwMS12R09lakE)

### Tools
- [Tech Stack](/handbook/business-ops/#tech-stack)
- [Tools and Tips](/handbook/tools-and-tips/)
- [Getting Started with Slack](https://get.slack.help/hc/en-us/articles/218080037-Getting-started-for-new-members)
- [Grovo](https://www.grovo.com/)
- [Outreach University](https://university.outreach.io/)
- [How to Use Outreach](https://docs.google.com/document/d/1FzvGEsL6ukxFZtk7ZkMUVAAT_wW-aVblAu1yOFWiEGo/edit)
- [Outreach Advanced Training](https://drive.google.com/open?id=0BxIvWVJUiX7AZGNWdEZ5MHpCOHc)
- [Drift Agent Training Guide](https://help.drift.com/getting-started/drift-agent-training-guide)

### Personal Development
- [Fanatical Prospecting](https://www.amazon.com/Fanatical-Prospecting-Jeb-Blount/dp/8126560053/ref=tmm_pap_swatch_0?_encoding=UTF8&qid=&sr=)
- [Predictable Revenue](https://www.amazon.com/Predictable-Revenue-Business-Practices-Salesforce-com/dp/0984380213)
- [The Challenger Sale](https://www.amazon.com/Challenger-Sale-Control-Customer-Conversation/dp/1591844355/ref=sr_1_1?ie=UTF8&qid=1516740978&sr=8-1&keywords=the+challenger+sale)
- [The Challenger Customer](https://www.amazon.com/Challenger-Customer-Selling-Influencer-Multiply/dp/1591848156/ref=sr_1_3?ie=UTF8&qid=1516740978&sr=8-3&keywords=the+challenger+sale)
- [Smart Calling: Eliminate the Fear, Failure, and Rejection from Cold Calling](https://www.amazon.com/Smart-Calling-Eliminate-Failure-Rejection-ebook/dp/B00C2BR56W)
- [Hacker News](https://news.ycombinator.com/)
- [Martin Fowler](https://martinfowler.com/)
- [The New Stack](https://thenewstack.io/)
- [Top 13 DevOps Blogs You Should Be Reading](https://stackify.com/top-devops-blogs/)

## Asking questions

Don't hesitate to ping one of your colleagues with a question, or someone on the team who specializes in what you're searching for. Everyone is happy to help, and it's better to get your work done faster with your team, than being held up at a road block.
- [Who to Talk to for What](/handbook/product/#who-to-talk-to-for-what)

## Improvements/Contributing

- If you get an answer to a question someone else may benefit from, incorporate it into this handbook or add it to the FAQ document
- After meetings or process changes, feel free to update this handbook, and submit a merge request.
- Create issues for any idea (small or large that), that you want feedback on
- All issued and MRs for changes to the SDR handbook assign to Chet Backman
