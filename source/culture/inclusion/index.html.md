---
layout: markdown_page
title: "Inclusion"
---

![Our Global Team](/images/team-photo-mexico-summit.jpg){: .illustration}*<small>In January 2017, our team of 150 GitLabbers from around the world!</small>*

### GitLab's Commitment to Employee Inclusion and Development

GitLab recognizes that to build a diversity initiative that resonates for our organization, we need to redefine it in a way that embodies our values and reinforces our global workforce. We also know that many [diversity programs fail](https://hbr.org/2016/07/why-diversity-programs-fail). Rather than focusing on building diversity as a collection of activities, data, and metrics, we're choosing [to build and institutionalize](http://www.russellreynolds.com/en/Insights/thought-leadership/Documents/Diversity%20and%20Inclusion%20GameChangers%20FINAL.PDF) a culture that is inclusive and supports all employees equally to achieve their professional goals. We will refer to this intentional culture curation as inclusion and development (i & d).


### A Snapshot of GitLabber's Identities

We've asked employees to voluntarily share their identities so that we can understand more about the makeup of our organization. While diversity data can help us learn more about the effectiveness of our i & d activities and can act as a measurement of the success of our culture, we won't rely solely on this data to build our teams.

#### GitLab Identity Data

Data below is as of 2018-01-05.

##### Country Specific Data

| Country Information                       | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 208   | 100%        |
| Based in the US                           | 111   | 53.37%      |
| Based in the UK                           | 18    | 8.65%       |
| Based in the Netherlands                  | 10    | 4.81%       |
| Based in Other Countries                  | 69    | 33.17%      |

##### Gender Data

| Gender (All)                              | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 208   | 100%        |
| Men                                       | 170   | 81.73%      |
| Women                                     | 38    | 18.27%      |

| Gender in Leadership                      | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 24    | 100%        |
| Men in Leadership                         | 20    | 83.33%      |
| Women in Leadership                       | 4     | 16.67%      |

| Gender in Development                     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 98    | 100%        |
| Men in Development                        | 87    | 88.78%      |
| Women in Development                      | 11    | 11.22%      |

##### Race/Ethnicity Data

| Race/Ethnicity (US Only)                  | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 111   | 100%        |
| Asian                                     | 8     | 7.21%       |
| Black or African American                 | 2     | 1.80%       |
| Hispanic or Latino                        | 6     | 5.41%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.90%       |
| Two or More Races                         | 1     | 0.90%       |
| White                                     | 64    | 57.66%      |
| Unreported                                | 29    | 26.13%      |

| Race/Ethnicity in Development   (US Only) | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 24    | 100%        |
| Asian                                     | 4     | 16.67%      |
| Black or African American                 | 1     | 4.17%       |
| White                                     | 14    | 58.33%      |
| Unreported                                | 5     | 20.83%      |

| Race/Ethnicity in Leadership (US Only)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 16    | 100%        |
| Asian                                     | 1     | 6.25%       |
| Native Hawaiian or Other Pacific Islander | 1     | 6.25%       |
| White                                     | 9     | 56.25%      |
| Unreported                                | 5     | 31.25%      |

| Race/Ethnicity (Global)                   | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 208   | 100%        |
| Asian                                     | 12    | 5.77%       |
| Black or African American                 | 5     | 2.40%       |
| Hispanic or Latino                        | 11    | 5.29%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.48%       |
| Two or More Races                         | 2     | 0.96%       |
| White                                     | 105   | 50.48%      |
| Unreported                                | 72    | 34.62%      |

| Race/Ethnicity in Development (Global)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 98    | 100%        |
| Asian                                     | 6     | 6.12%       |
| Black or African American                 | 3     | 3.06%       |
| Hispanic or Latino                        | 4     | 4.08%       |
| Two or More Races                         | 1     | 1.02%       |
| White                                     | 46    | 46.94%      |
| Unreported                                | 38    | 38.78%      |

| Race/Ethnicity in Leadership (Global)     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 24    | 100%        |
| Asian                                     | 1     | 4.17%       |
| Native Hawaiian or Other Pacific Islander | 1     | 4.17%       |
| White                                     | 12    | 50.00%      |
| Unreported                                | 10    | 41.67%      |

##### Age Distribution

| Age Distribution (Global)                 | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 208   | 100%        |
| 20-24                                     | 22    | 10.58%      |
| 25-29                                     | 59    | 28.37%      |
| 30-34                                     | 50    | 24.04%      |
| 35-39                                     | 27    | 12.98%      |
| 40-49                                     | 33    | 15.87%      |
| 50-59                                     | 13    | 6.25%       |
| 60+                                       | 1     | 0.48%       |
| Unreported                                | 3     | 1.44%       |

### 100% remote

We have 100% remote team which means our applicants are not limited by geography – Our door, if you will, is open to everyone and we [champion this approach](http://www.remoteonly.org/), to the extent that it’s possible, for all companies.

### Technical interviews on real issue

Another practice we employ is to ask GitLab applicants to work on real issues as a part of our hiring process. This allows people who may not stand out well on paper - which is what the hiring process traditionally depends on - to really show us what they can do and how they approach issues. For us, that’s proven more valuable than where an applicant studied or has worked before.

### Diversity Sponsorship program

One of our biggest initiatives is the [GitLab Diversity Sponsorship program](https://about.gitlab.com/community/sponsorship/). This is a sponsorship opportunity we offer to any “diversity in tech” event globally. We offer funds to help support the event financially and, if the event is in a city we have a GitLab team member, we get hands-on by offering to coach and/or give a talk whenever possible.

### Values

We try to make sure [our values](https://about.gitlab.com/handbook/values/) reflect our goal to be inclusive.

### Double referral bonus

We want to encourage and support diversity on our team and in our hiring practices, so we will offer a [$2000 referral bonus](https://about.gitlab.com/handbook/incentives/#referral-bonuses) for hires from underrepresented groups in the tech industry for engineering roles at GitLab. This underrepresented group is defined as: women, African-Americans, Hispanic-Americans/Latinos, and veterans.

### Inclusive benefits

We list our [Transgender Medical Services](https://about.gitlab.com/handbook/benefits/#transgender-medical-services) and [Pregnancy & Maternity Care](https://about.gitlab.com/handbook/benefits/#pregnancy--maternity-care) publicly so people don't have to ask for them during interviews.

### Inclusive language

In our [general guidelines](https://about.gitlab.com/handbook/general-guidelines/) we list: 'Use inclusive language. For example, prefer "Hi everybody" or "Hi people" to "Hi guys".'

### Inclusive interviewing

As part of [our interviewing process](https://about.gitlab.com/handbook/hiring/interviewing/) we list: "The candidate should be interviewed by at least one female GitLab team member."

### Provide resources
[Collect, share, and distribute inclusion trainings for all employees.](https://about.gitlab.com/culture/inclusion/resources)

### 2017 inclusion goals

1. [Rewrite 100% of vacancy descriptions](https://gitlab.com/gitlab-com/peopleops/issues/333) for inclusive language
1. Publish 4 culture focused, [employee written blogs](https://gitlab.com/gitlab-com/www-gitlab-com/issues/1291)
1. Add inclusion + unconscious bias [training to onboarding](https://gitlab.com/gitlab-com/peopleops/merge_requests/42#8f86e8978ec34dbe161ba081c23d509c2d0877ac) for all employees

### Never done

We recognize that having an inclusive organization is never done. If you work at GitLab please join our #inclusion chat channel. If you don't work for us please email brittany@gitlab.com with suggestions and concerns.
